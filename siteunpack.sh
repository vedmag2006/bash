#!/bin/bash
# set -e
# set -x

echo "Version 1.1"

# configs
BASE_PATH="/var/www"
###

time_start=$(date +%s)

DOMAIN="$1"
NEW_DOMAIN="$DOMAIN"

function func_check_args {
  if [ ! -f "/usr/local/bin/wp" ]; then
    echo " --- wp_cli отсутствует --"
    exit 1
  fi
  if [ -z $1 ]  ; then
    echo -e " --- не задан сайт [$1]\n -- exit 1"
    exit 1
  fi
  if [ ! -z $2 ]  ; then
    NEW_DOMAIN=$2
  fi
  if [ ! -d "/root/site_pack/$1" ]; then
    echo -e " --- Пустая дирректория [/root/site_pack/$1]\n -- exit 1"
    exit 1
  fi
  if [ ! -f "/root/site_pack/$1/mysqldump.sql.gz" ]; then
    echo -e " --- Нету файла [/root/site_pack/$1/mysqldump.sql.gz]\n -- exit 1"
    exit 1
  elif [ ! -f "/root/site_pack/$1/files.tgz" ]; then
    echo -e " --- Нету файла [/root/site_pack/$1/files.tgz]\n -- exit 1"
    exit 1
  fi
  if [ -d "$BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN.backup" ]; then
  echo -e " --- Администратор проверь папку, она не пустая [$BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN.backup]\n -- exit 1"
  exit 1
  fi
}

func_check_args "$1" "$2"

if [ ! -n "dpkg -l | grep pv" ]; then
        echo "pv not installed!"
        apt install pv
fi

echo -e " --- Создаем нового пользователя если нужен --"
ISP_USER="$(echo "$NEW_DOMAIN" | sed -e 's/\./_/g' -e 's/\-/_/g')"
USER_PASS="$(head -c 100 /dev/urandom | base64 | sed 's/[+=/A-Z]//g' | tail -c 13)"
echo -e "\tISP_USER=$ISP_USER"
check_user=(`/usr/local/mgr5/sbin/mgrctl -m ispmgr user | grep -o "name=$ISP_USER"`)
  if [[ ! "$check_user" == "name=$ISP_USER" ]]; then
    echo -e " --- Пользователя нет, создаем --"
    /usr/local/mgr5/sbin/mgrctl -m ispmgr user.add \
        name=$ISP_USER \
        fullname=$NEW_DOMAIN \
        passwd=$USER_PASS \
        confirm=$USER_PASS \
        limit_php_mode=php_mode_fcgi_nginxfpm \
        limit_php_mode_fcgi_nginxfpm=on \
        limit_php_mode_mod=on \
        limit_ssl=on \
        addinfo=off \
        limit_shell=off \
        redirect_http=on\
        sok=ok
  else
    echo -e " --- Пользователь существует --"
    exit
  fi

echo -e " --- Добавляем сайт --"
echo -e "\tDOMAIN=$NEW_DOMAIN"
check_site=(`/usr/local/mgr5/sbin/mgrctl -m ispmgr webdomain | grep -o "name=$NEW_DOMAIN"`)
  if [[ ! "$check_site" == "name=$NEW_DOMAIN" ]]; then
    echo -e " --- Сайта нет, создаем --"
    /usr/local/mgr5/sbin/mgrctl -m ispmgr webdomain.edit \
        owner=$ISP_USER \
        name=$NEW_DOMAIN \
        aliases=www.$NEW_DOMAIN \
        secure=on \
        php_mode=php_mode_fcgi_nginxfpm \
        php_enable=on \
        email=webmaster@$NEW_DOMAIN \
        srv_gzip=on \
        gzip_level=3 \
        srv_cache=on \
        expire_times=expire_times_h \
        expire_period=24 \
        redirect_http=on \
        sok=ok
   else
    echo -e " --- Сайт существует --"
    exit
  fi
echo

echo -e ' --- Очищаем папку сайта --'
mv $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN.backup
echo -e "\t$(du -sh $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN.backup)"
echo

echo -e " --- Распаковываем файлы сайта --"
pv /root/site_pack/$DOMAIN/files.tgz | gunzip | tar xzp -C $BASE_PATH/$ISP_USER/data/www/
echo -e "\t$(ls -la $BASE_PATH/$ISP_USER/data/www/$DOMAIN/)"
echo

echo -e " --- Переименовываем папку --"
mv $BASE_PATH/$ISP_USER/data/www/$DOMAIN  $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN
chown -R $ISP_USER:$ISP_USER $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN
echo -e "$BASE_PATH/$ISP_USER/data/www/$DOMAIN > $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN"
echo -e "\t$(ls -la $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/)"
echo

echo -e " --- Правим конфиг сайта, создав копию _old"
DB_PASS="$(head -c 100 /dev/urandom | base64 | sed 's/[+=/A-Z]//g' | tail -c 13)"
echo -e "\t --- Пароль БД - $DB_PASS"
cp $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-config.php $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-config.php_old
sed -i -e  "s/.*DB_NAME.*/define( 'DB_NAME', '$ISP_USER' );/" $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-config.php
sed -i -e  "s/.*DB_USER.*/define( 'DB_USER', '$ISP_USER' );/" $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-config.php
sed -i -e  "s/.*DB_PASSWORD.*/define( 'DB_PASSWORD', '$DB_PASS' );/" $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-config.php
echo

echo -e " --- Проверяем наличие DB и при необходимости создаем --"
check_base=(`/usr/local/mgr5/sbin/mgrctl -m ispmgr db | grep -o "name=$ISP_USER"`)
  if [[ ! "$check_base" == "name=$ISP_USER" ]]; then
    echo -e " --- Базы нет, создаем --"
    /usr/local/mgr5/sbin/mgrctl -m ispmgr db.edit \
        hide_remote_access=on \
        name=$ISP_USER \
        username=$ISP_USER \
        password=$DB_PASS \
        confirm=$DB_PASS \
        remote_access=off \
        addr_list= \
        owner=$ISP_USER \
        server=MySQL \
        charset=utf8 \
        user=* \
        saved_filters= \
        sok=ok
   else
    echo -e " --- База существует --"
    exit
  fi

echo -e " --- Импортируем дамп в новую базу --"
zcat /root/site_pack/$DOMAIN/mysqldump.sql.gz | mysql -u$ISP_USER -p$DB_PASS $ISP_USER
echo

echo -e " --- Отключение клоаки --"
if [[ `find $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-content/plugins/cloak-service-client/cloak-service/ -name "config.php"` ]]; then
sed -i '/'baseUrl'/s/^/#/' $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-content/plugins/cloak-service-client/cloak-service/config.php
sed -i '/'campaignCode'/s/^/#/' $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-content/plugins/cloak-service-client/cloak-service/config.php
fi
echo

echo -e " --- Меняем данные в базе --"
wp --path=$BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/ search-replace $DOMAIN $NEW_DOMAIN  --allow-root
chown -R $ISP_USER:$ISP_USER $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/

echo -e " --- Включение клоаки --"
if [[ `find $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-content/plugins/cloak-service-client/cloak-service/ -name "config.php"` ]]; then
sed -i '/'baseUrl'/s/^#\+//' $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-content/plugins/cloak-service-client/cloak-service/config.php
sed -i '/'campaignCode'/s/^#\+//' $BASE_PATH/$ISP_USER/data/www/$NEW_DOMAIN/wp-content/plugins/cloak-service-client/cloak-service/config.php
fi
echo

echo -e " --- Проверяем наличие Let's Encrypt --"
  if [ ! $(/usr/local/mgr5/sbin/mgrctl -m ispmgr sslcert | grep $NEW_DOMAIN | grep  "letsencrypt=on" > /dev/null) ] ; then
echo

echo -e " --- Заказываем Let's Encrypt --"
    /usr/local/mgr5/sbin/mgrctl -m ispmgr letsencrypt.generate \
    crtname=$NEW_DOMAIN-le1 \
    domain=$NEW_DOMAIN  www.$NEW_DOMAIN \
    email=webmaster@$NEW_DOMAIN \
    enable_cert=on \
    wildcard=off \
    dns_check=off \
    hide_dns= \
    from_webdomain=$NEW_DOMAIN \
    username=$ISP_USER \
    domain_name=$NEW_DOMAIN \
    keylen=2048 \
    saved_filters= \
    sok=ok
  fi
echo

echo -e " --- Удаляем самоподписной сертификат --"
    /usr/local/mgr5/sbin/mgrctl -m ispmgr sslcert.delete \
    elid=$ISP_USER%#%$NEW_DOMAIN \
    sok=ok
    echo

echo -e " --- Создаем hosts в /root/site_pack --"
    IP="$(/usr/local/mgr5/sbin/mgrctl -m ispmgr webdomain | grep "$NEW_DOMAIN" | sed 's/=/ /g' | awk '{print $20}')"
    echo $IP $NEW_DOMAIN >> /root/site_pack/hosts
    echo

time_finish=$(date +%s)
let time_spend=$time_finish-$time_start
echo -e " --- [ finish ]"
echo -e "\ttime spend: $time_spend second"
echo