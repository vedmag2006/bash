echo ${ANSIBLE_VAULT_SECRET} > vault_secret

INI_FILE=$INI_FILE
NUMBER_HOSTS=
NAME=

# сколько сейчас ТДС у клиента
HOSTS_NOW=(`grep ^"$NAME"_tds_ ./$INI_FILE | wc -l`) 

# сколько ТДС будем запрашивать - есть ненулевая вероятность что на самых свободных хостах уже есть TDS
TOTAL_SERCHED=$(($HOSTS_NOW + $NUMBER_HOSTS)) 

# список из самых свободных хостов
curl -s 'http://10.164.0.125:8481/select/0/prometheus/api/v1/query?query=bottomk('$TOTAL_SERCHED',node_load5\{instance=~"tds-.*"\})'| jq -r '.data.result'| jq '.[].metric.instance' | sed -e 's/"//g' >> QWERY.txt

# список из хостов где уже есть ТДС
grep $NAME $INI_FILE | awk '{print $2}' | while read -r line; do grep $line hosts_gcloud.ini | awk '{print $1}'  >> LIST_NOW.txt; done

# Убираем из списка те хосты, где уже есть ТДС
cat LIST_NOW.txt | while read -r line; do sed -i "/$line/d" QWERY.txt; done

#
cat QWERY.txt

#
COUNT=(`grep ^"$NAME"_tds_ ./$INI_FILE | wc -l`) 
cat QWERY.txt | while read -r line; do 
  let COUNT=$COUNT+1 
    if [[ $COUNT -le $TOTAL_SERCHED ]]; then 
      grep $line hosts_tds_vm.ini | awk -v awkname="$NAME" -v awkcount=$COUNT '{print awkname "_tds_" awkcount " " $2  " ansible_user=root gotds_consul_name="awkname " " $4 " " "gotds_count_per_node=1 without_webmaster=true without_api=true without_admin=true" >> "$INI_FILE"}' 
    fi 
done
echo $COUNT

#
LIMIT=$(cat QWERY.txt |  while read -r line; do 
  let COUNT=COUNT+1  
    if [[ $COUNT -le $TOTAL_SERCHED ]]; then  
      grep $line hosts_tds_vm.ini | awk -v awkname="$NAME" -v awkcount=$COUNT '{print awkname "_tds_" awkcount}'; 
    fi 
done | tr '\n' ',')
echo $LIMIT

rm -f QWERY.txt
rm -f LIST_NOW.txt

#
ansible-playbook -i $INI_FILE --diff -u root --key-file=$sshkey -e @secrets/secrets.yml -e serial_number=50 site_cloud.yml --limit "$LIMIT" -t tds --skip-tags infra --vault-password-file vault_secret


git config --global user.email "bot@affisecorp.com" && 
git config --global user.name "bot@affisecorp.com" && \
git add -A . && \
git commit -m "Add new $NUMBER_HOSTS tds for client $NAME" && \
git pull origin master && \
git push origin master && \
git pull origin master

rm -f vault_secret