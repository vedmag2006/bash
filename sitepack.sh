#!/bin/bash
#
# configs
echo "Version 1.0"

BASE_PATH="/var/www"
TMP_DIR="/root/site_pack"
###

# TO DO
# - проверить свободное место на диске перед созданием архива
# - использовать сниженый nice на процесс архивации
# - переписать на python
# - реализовать в виде сервиса с REST API и запуск поралельных воркеров для работы

time_start=$(date +%s)

function func_check_args {
  if [ -z $1 ]  ; then
    echo -e " -- не задан сайт [$1]\n -- exit 1"
    exit 1
  fi
}

func_check_args $1
DOMAIN="$1"
mkdir -p $TMP_DIR/$DOMAIN/

echo -e " -- ищем папку сайта"
SITE_PATH=$(/usr/local/mgr5/sbin/mgrctl -m ispmgr webdomain | grep "$DOMAIN" | awk '{ print $3 }' | sed 's/docroot=//g')
#SITE_PATH=$(find $BASE_PATH -maxdepth 4 -type d -name "$DOMAIN")
echo -e "\tSITE_PATH=$SITE_PATH"
echo

echo -e " -- читаем данные из wp-config.php"
DB_NAME=$(cat $SITE_PATH/wp-config.php| grep "DB_NAME" | tr "'" '"' | cut -d'"' -f4)
echo -e "\tDB_NAME=$DB_NAME"
DB_USER=$(cat $SITE_PATH/wp-config.php| grep "DB_USER" | tr "'" '"' | cut -d'"' -f4)
echo -e "\tDB_USER=$DB_USER"
DB_PASSWORD=$(cat $SITE_PATH/wp-config.php| grep "DB_PASSWORD" | tr "'" '"' | cut -d'"' -f4)
echo -e "\tDB_PASSWORD=$DB_PASSWORD"
DB_HOST=$(cat $SITE_PATH/wp-config.php| grep "DB_HOST" | tr "'" '"' | cut -d'"' -f4)
echo -e "\tDB_HOST=$DB_HOST"
echo

echo -e " -- вычисляем размер mysql базы"
query="SELECT table_schema as 'Database', \
       SUM(data_length + index_length) AS 'Size_Byte' \
       FROM information_schema.TABLES \
       WHERE table_schema = '$DB_NAME'\G"
DB_SIZE=$(mysql -h$DB_HOST -u$DB_USER -p$DB_PASSWORD -e "$query" | awk '/Size_Byte/ {print $NF}')
echo -e "\tDB_SIZE=$DB_SIZE bytes"
echo

echo -e " -- выполняем mysqldump"
mysqldump -h$DB_HOST -u$DB_USER -p$DB_PASSWORD $DB_NAME |\
  pv -s $DB_SIZE |\
  gzip > $TMP_DIR/$DOMAIN/mysqldump.sql.gz
echo -e "\t$(du -sh $TMP_DIR/$DOMAIN/mysqldump.sql.gz)"
echo

echo -e " -- архивируем файлы сайта"
cd $SITE_PATH/../
tar -czf - $DOMAIN | pv -s $(du -sb $DOMAIN | cut -f1) | gzip > $TMP_DIR/$DOMAIN/files.tgz
echo -e "\t$(du -sh $TMP_DIR/$DOMAIN/files.tgz)"
echo

time_finish=$(date +%s)
let time_spend=$time_finish-$time_start
echo -e " -- [ finish ]"
echo -e "\ttime spend: $time_spend second"
echo